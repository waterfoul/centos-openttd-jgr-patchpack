FROM centos:latest
MAINTAINER Aaron Aichlmayr <waterfoul@gmail.com>
RUN yum update -y && yum clean all
RUN yum install -y SDL zlib xz lzo screen && yum clean all

RUN yum install -y make gcc-c++ SDL-devel zlib-devel subversion esound xz-devel lzo-devel && \
	curl -L https://github.com/JGRennison/OpenTTD-patches/archive/jgrpp-0.15.1.tar.gz -o out.tar.gz && \
	tar -xvf out.tar.gz && \
	cd OpenTTD-* && \
	./configure && \
	make --jobs=$(expr `getconf _NPROCESSORS_ONLN` + 1) && \
	mv bin /openttd && \
	cd .. && \
	rm -rf OpenTTD-* && \
	rm -f out.tar.gz && \
	yum remove -y make gcc-c++ SDL-devel zlib-devel subversion xz-devel lzo-devel && yum clean all && \
	yum autoremove -y && \
	yum clean all
	
RUN yum install -y unzip && \
	curl -L https://binaries.openttd.org/extra/opengfx/0.5.2/opengfx-0.5.2-all.zip -o opengfx.zip && \
	unzip opengfx.zip && \
	rm -f opengfx.zip && \
	mv opengfx* /openttd/baseset/ && \
	yum remove -y unzip && \
	yum autoremove -y && \
	yum clean all
	
VOLUME '/openttd-data'

EXPOSE 3979 3979/udp

ENTRYPOINT ["/bin/bash", "/start.sh"]

ADD toOpenTTD.sh /toOpenTTD.sh
ADD start.sh /start.sh