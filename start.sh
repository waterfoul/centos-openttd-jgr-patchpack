cd /openttd-data

# Stops the server
openttd_stop() {
	# Stops the server
	sleep 10
	echo "Tries to stop server..."
	openttd_save
	sleep 1s
	# Sending "exit" to the admin consol
	openttd_toConsol exit
	sleep 0.5
	# Waiting for the server to stop
	sec=0
	isInStop=1
	while is_running
	do
		sleep 1 
		sec=$sec+1
		if [[ $sec -eq 5 ]]
		then
			echo "Server using longer time then expected to shutdown. Lets wait 60 sec before abort..."
		fi
		if [[ $sec -ge 60 ]]
		then
			echo "Could not stop server during this 60 sec"
			exit 1
		fi
	done
	rm /openttd.pid
	unset isInStop
	is_running
}

# Checks if openttd screen exist.
is_running() {

	if [ -r "/openttd.pid" ]
	then
		pid=$(head -1 /openttd.pid)
		if ps ax | grep -v grep | grep ${pid} | grep "openttd" > /dev/null
		then
			return 0
		else 
			if [ -z "$isInStop" ]
			then
				if [ -z "$roguePrinted" ]
				then
					roguePrinted=1
					#echo "Pidfile found!"
				fi
			fi
			return 1
		fi
	elsescript --help
		if ps ax | grep -v grep | grep "openttd ${SERVICE}" > /dev/null
		then
			echo "No pidfile found, but server seems to be running."
			echo "Trying to creating new pidfile."
			
			pid=$(ps ax | grep -v grep | grep "openttd ${SERVICE}" | cut -f1 -d' ')
			check_permissions
			echo $pid > /openttd.pid

			return 0
		else
			return 1
		fi
	fi
}

openttd_toConsol() {
	if is_running
	then
			screen -p 0 -S openttd -X stuff "$1 $2"`echo -ne "\015"`
	else
			echo "OPENTTD not running.."
	fi
}
openttd_save() {
	openttd_toConsol save /openttd-data/game
}
openttd_load() {
	openttd_toConsol load /openttd-data/game
}
echo "/openttd/openttd -D -c /openttd-data/game.cfg" "$@" > /start_openttd
chmod +x /start_openttd

screen -wipe openttd 2>&1 1>/dev/null
screen -dmS openttd script -f /openttd.log -c /start_openttd
screen -list | grep '\.openttd' | cut -f1 -d'.' > /openttd.pid

sec=0
until is_running 
do
	sleep 1
	sec=$sec+1
	if [[ $sec -eq 10 ]]
	then
		echo "Server taking a long time to start... Lets wait 60 sec before abort..."
	fi
	if [[ $sec -ge 60 ]]
	then
		echo "Failed to start, aborting."
		exit 1
	fi
done	

tail -F /openttd.log &
openttd_load

while true
do
	sleep 1h
	openttd_save
done

trap "openttd_stop" SIGTERM